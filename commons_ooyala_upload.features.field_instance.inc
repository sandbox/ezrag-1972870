<?php
/**
 * @file
 * commons_ooyala_upload.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function commons_ooyala_upload_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-ooyala_video-body'
  $field_instances['node-ooyala_video-body'] = array(
    'bundle' => 'ooyala_video',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => '0',
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => '31',
    ),
  );

  // Exported field_instance: 'node-ooyala_video-field_ooyala_video_upload'
  $field_instances['node-ooyala_video-field_ooyala_video_upload'] = array(
    'bundle' => 'ooyala_video',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'ooyala',
        'settings' => array(),
        'type' => 'ooyala_default',
        'weight' => '1',
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'display_in_partial_form' => 1,
    'entity_type' => 'node',
    'field_name' => 'field_ooyala_video_upload',
    'label' => 'Video file',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'ooyala_upload',
      'settings' => array(),
      'type' => 'ooyala_upload',
      'weight' => '42',
    ),
  );

  // Exported field_instance: 'node-ooyala_video-title_field'
  $field_instances['node-ooyala_video-title_field'] = array(
    'bundle' => 'ooyala_video',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'display_in_partial_form' => 1,
    'entity_type' => 'node',
    'field_name' => 'title_field',
    'label' => 'Title',
    'required' => 1,
    'settings' => array(
      'hide_label' => array(
        'entity' => 0,
        'page' => 0,
      ),
      'text_processing' => '0',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => '60',
      ),
      'type' => 'text_textfield',
      'weight' => -5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Title');
  t('Video file');

  return $field_instances;
}
