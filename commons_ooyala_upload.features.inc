<?php
/**
 * @file
 * commons_ooyala_upload.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function commons_ooyala_upload_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function commons_ooyala_upload_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function commons_ooyala_upload_node_info() {
  $items = array(
    'ooyala_video' => array(
      'name' => t('Ooyala Video'),
      'base' => 'node_content',
      'description' => t('Upload a video to the Ooyala service.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
