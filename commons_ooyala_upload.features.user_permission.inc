<?php
/**
 * @file
 * commons_ooyala_upload.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function commons_ooyala_upload_user_default_permissions() {
  $permissions = array();

  // Exported permission: create ooyala_video content.
  $permissions['create ooyala_video content'] = array(
    'name' => 'create ooyala_video content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ooyala_video content.
  $permissions['delete any ooyala_video content'] = array(
    'name' => 'delete any ooyala_video content',
    'roles' => array(
      'content moderator' => 'content moderator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ooyala_video content.
  $permissions['delete own ooyala_video content'] = array(
    'name' => 'delete own ooyala_video content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ooyala_video content.
  $permissions['edit any ooyala_video content'] = array(
    'name' => 'edit any ooyala_video content',
    'roles' => array(
      'content moderator' => 'content moderator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ooyala_video content.
  $permissions['edit own ooyala_video content'] = array(
    'name' => 'edit own ooyala_video content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: upload ooyala videos.
  $permissions['upload ooyala videos'] = array(
    'name' => 'upload ooyala videos',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'ooyala',
  );

  return $permissions;
}
