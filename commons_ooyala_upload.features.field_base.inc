<?php
/**
 * @file
 * commons_ooyala_upload.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function commons_ooyala_upload_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_ooyala_video_upload'
  $field_bases['field_ooyala_video_upload'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_ooyala_video_upload',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => '0',
    'module' => 'ooyala',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'ooyala',
  );

  return $field_bases;
}
